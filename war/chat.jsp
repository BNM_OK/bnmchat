<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>

<head>
<Title>Chat</Title>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript">
	var auto_refresh = setInterval(function() {
		$('#load_me').load('/get').fadeIn("slow");
	}, 500);
</script>
<script type="text/javascript">
	var auto_refresh = setInterval(function() {
		$('#load_users').load('/activeUsers').fadeIn("slow");
	}, 5000);
</script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>
	<c:if test="${login eq null}">
		<c:set var="login" value="${param.login}" scope="session" />
	</c:if>
	<c:if test="${login eq null}">
		<c:redirect url="index.html" />
	</c:if>
	<a class="btn btn-link" href="/exit" role="button">Sign out</a>
	<div class="container" style="background-color: lightsteelblue;">
		<h1>Main Chat</h1>
		<p>Messages:</p>
		<div class="row">
			<div class="col-sm-8" style="background-color: lightsteelblue;">
				<div class="form-group">
					<select id="load_me" multiple class="form-control"
						style="height: 325px; max-height: 325px; width: 737px; max-width: 737px;">
					</select>
				</div>
				<div class="col-lg-12">
					<form class="bs-example bs-example-form" role="form" action="/add"
						method="POST">
						<div class="input-group">
							<span class="input-group-addon">To:</span> <input type="text"
								class="form-control" placeholder="Name" name="to">
						</div>
						<br>

						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit">Send!</button>
							</span> <input type="text" class="form-control" name="message"
								value="${login}:" />
						</div>
					</form>
				</div>
			</div>

			<div class="col-sm-4" style="background-color: lightsteelblue;">
				<form class="bs-example bs-example-form" role="form"
					action="/chatroom" method="POST">
					<div class="form-group">
						<label for="sel2">Active users(hold shift to select more
							than one to create chatroom:</label> <select id="load_users" multiple
							class="form-control" id="sel2" name="users" size="20">
						</select>
						<button type="submit" class="btn btn-default">Create Chat
							Room</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
