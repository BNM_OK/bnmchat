<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
<Title>Chat</Title>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript">
	var auto_refresh = setInterval(function() {
		$('#load_me').load('/getChatRoomMessages').fadeIn("slow");
	}, 500);
</script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>

<body>

	<a class="btn btn-link" href="/exit" role="button">Sign out</a>
	<div class="container" style="background-color: palegreen;">
		<h1>ChatRoom</h1>
		Messages:
		<div class="row">
			<div class="col-sm-8" style="background-color: palegreen;">
				<div class="form-group">
					<select id="load_me" multiple class="form-control"
						style="height: 325px; max-height: 325px; width: 737px; max-width: 737px;">
					</select>
				</div>
				<div class="col-lg-12">
					<form class="bs-example bs-example-form" role="form"
						action="/addChatRoom" method="POST">
						<div class="input-group">
							<span class="input-group-addon">To:</span> <input type="text"
								class="form-control" value="Chatroom" name="to">
						</div>
						<br>

						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit">Send!</button>
							</span> <input type="text" class="form-control" name="message"
								value="${login}:">
						</div>
					</form>
				</div>
			</div>

			<div class="col-sm-4" style="background-color: palegreen;">
				<div class="panel panel-primary">
					<div class="panel-body">
						<c:forEach items="${users}" var="s">
							${s}<p>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
