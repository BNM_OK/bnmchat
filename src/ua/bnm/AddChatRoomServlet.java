package ua.bnm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddChatRoomServlet extends HttpServlet {
	
	private MessageList msgList = MessageList.getInstance();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Message m = null;

		try {
			m = Message.readFromClient(req);
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}

		if (m == null) {
			resp.setStatus(400); // bad request
			return;
		} else
			msgList.add(m);
//		redirect to new chatroom page
		resp.sendRedirect("/chatroom.jsp");
	}

}
