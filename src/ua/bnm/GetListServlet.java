package ua.bnm;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetListServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private MessageList msgList = MessageList.getInstance();

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		OutputStream os = resp.getOutputStream();

		List<Message> list = msgList.get();
		String login = (String) req.getSession().getAttribute("login");

		Message.writeToClient(os, list, login);

	}

}
