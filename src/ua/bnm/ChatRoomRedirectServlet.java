package ua.bnm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChatRoomRedirectServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String[] users = req.getParameterValues("users");
		req.setAttribute("users", users);
		req.getSession().setAttribute("users", users);
		resp.sendRedirect("/chatroom.jsp");
	}
}
