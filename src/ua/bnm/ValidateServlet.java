package ua.bnm;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ValidateServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserList auth = UserList.getInstance();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String login = req.getParameter("login");
		String password = req.getParameter("pwd");
		if (!validator(login, password)) {
			resp.setStatus(401); // unauthorized
			return;
		}
		req.getSession().setAttribute("login", login);
		req.getRequestDispatcher("chat.jsp").forward(req, resp);

	}

	private boolean validator(String login, String password) {
		if (login == null || password == null)
			return false;
		String pass = auth.getUsers().get(login);
		if (pass == null) {
			auth.add(login, password);
			return true;
		} else if (pass.equals(password)) {
			return true;
		}
		return false;
	}

}
