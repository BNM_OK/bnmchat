package ua.bnm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetChatRoomMessagesServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MessageList msg = MessageList.getInstance();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String[] users = (String[]) req.getSession().getAttribute("users");
		String login = (String) req.getSession().getAttribute("login");
		if (users == null || login == null) {
			resp.setStatus(400);
			return;
		}
		byte[] buf = prepareMessages(login, users);
		resp.getOutputStream().write(buf);
	}

	private byte[] prepareMessages(String login, String[] users) {
		StringBuilder sb = new StringBuilder();
		List<Message> list = msg.get();
		for (Message message : list) {
			for (String user : users) {
				if (message.from.equals(user) || message.to.equals(login)) {
					sb.append(message.toString());
					sb.append("<br>");
				}
			}
		}

		try {
			return sb.toString().getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
