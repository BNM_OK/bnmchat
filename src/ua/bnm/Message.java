package ua.bnm;

import java.io.*;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

public class Message {

	public Date date = new Date();
	public String from;
	public String to;
	public String text;

	@Override
	public String toString() {
		return new StringBuilder().append("[").append(date.toString())
				.append(", From: ").append(from).append(", To: ").append(to)
				.append("] ").append(text).toString();
	}

	public static void writeToClient(OutputStream out, List<Message> list,
			String login) throws IOException {
		StringBuilder sb = new StringBuilder();

		for (Message message : list) {

			if (message.to.equals(login) || message.to.equals("")) {
				sb.append("<option>");
				sb.append(message.toString());
				sb.append("</option>");
			}
		}
		out.write(sb.toString().getBytes("UTF-8"));
	}

	public static Message readFromClient(HttpServletRequest req)
			throws IOException, ClassNotFoundException {
		Message msg = new Message();
		String to = req.getParameter("to");
		String[] splited_msg = req.getParameter("message").split(":");

		if (to != null)
			msg.to = to;
		if (splited_msg.length > 1) {
			msg.from = splited_msg[0];
			msg.text = splited_msg[1];
		} else {
			msg.from = "Anonymus";
			msg.text = splited_msg[0];
		}

		return msg;

	}

}
