package ua.bnm;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoadActiveUsersServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	UserList users = UserList.getInstance();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		StringBuilder sb = new StringBuilder();
		Map<String, String> usersMap = users.getUsers();
		for (String login : usersMap.keySet()) {
			sb.append("<option>");
			sb.append(login);
			sb.append("</option>");
		}
		byte[] buf = sb.toString().getBytes("UTF-8");
		resp.getOutputStream().write(buf);

	}

}
