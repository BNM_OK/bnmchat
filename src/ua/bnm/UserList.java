package ua.bnm;

import java.util.HashMap;
import java.util.Map;

public class UserList {
	private static final UserList auth = new UserList();

	private final Map<String, String> users = new HashMap<String, String>();

	private UserList() {
	}

	public static UserList getInstance() {
		return auth;
	}

	public synchronized void add(String key, String value) {
		users.put(key, value);
	}

	public synchronized Map<String, String> getUsers() {
		return users;

	}

}
